//
//  Colors.swift
//  PravinTravels
//
//  Created by IIPL 5 on 15/01/19.
//  Copyright © 2019 IIPL 5. All rights reserved.
//

import UIKit

class Colors: NSObject {

    static let whiteColor = UIColor.white
    static let appThemeColor = UIColor(red: 22.0/255.0, green: 157.0/255.0, blue: 223.0/255.0, alpha: 1.0)
    static let statusBarColor = UIColor(red: 0.0/255.0, green: 138.0/255.0, blue: 205.0/255.0, alpha: 0.7)
    static let buttonThemeColor = UIColor(red: 38.0/255.0, green: 108.0/255.0, blue: 158.0/255.0, alpha: 1.0)
    static let toastMessageThemeColor = UIColor(red: 227.0/255.0, green: 67.0/255.0, blue: 67.0/255.0, alpha: 1.0)
    static let toastMessageLightGrayThemeColor = UIColor(red: 0.0/255.0, green: 128.0/255.0, blue: 0.0/255.0, alpha: 1.0)
}
