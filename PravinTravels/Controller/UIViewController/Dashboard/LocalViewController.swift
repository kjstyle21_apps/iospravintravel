//
//  LocalViewController.swift
//  PravinTravels
//
//  Created by IIPL 5 on 06/01/19.
//  Copyright © 2019 IIPL 5. All rights reserved.
//

import UIKit

class LocalViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    //MARK:- Variable Declaration
    
    @IBOutlet var mEndDateTextField: UITextField!
    @IBOutlet var mStartDateTextField: UITextField!
    @IBOutlet var mSelectTimeTextField: UITextField!
    @IBOutlet var mSearchCabButton: UIButton!
    @IBOutlet var mTableView: UITableView!
    
    let picker : UIDatePicker = UIDatePicker()
    let timePicker = UIDatePicker()
    var doneButton = UIButton()
    var daysListArray = [String]()
    
    var isFromStartDateButton = Bool()
    var isFromEndDateButton = Bool()
    var isSelectTimeDateButton = Bool()
    var start_date = Date()
    var end_date = Date()
    var selectedTime = ""
    
    var isStartDateCalenderOpen = true
    var isEndDateCalenderOpen = true
    var isTimePickerOpen = true
    var isEndDateSelected = false
    
    var sourceCityArray = [String]()
    var mutableDictForSourceCity = [String: Int]()
    var selectedSourceCityID = Int()
    var selectedSourceCityName = ""
    var isFromFinalSearchButton = Bool()
    
    //MARK:- ViewController LifeCycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.addDoneButton()
        self.doneButton.isHidden = true
        self.tabBarController?.tabBar.isHidden = false
        mSearchCabButton.layer.cornerRadius = 0.1 *
            mSearchCabButton.frame.size.height
        
        self.navigationController?.isNavigationBarHidden = false
        // Remove empty cell from tableview
        mTableView.tableFooterView = UIView(frame: .zero)
        
        let menuButton = self.navigationItem.leftBarButtonItem
        
        if self.revealViewController() != nil {
            menuButton?.target = self.revealViewController()
            menuButton?.action = #selector(SWRevealViewController.revealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        }
    }

    //MARK:- TableView DataSource and Delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "LocalTableViewCell")! as! LocalTableViewCell
        
        /*
        cell.mSelectCityTextField.font = cell.mSelectCityTextField.font?.withSize(16)
        cell.mSelectCityTextField.theme.font = UIFont.systemFont(ofSize: 16)
        
        cell.mSelectCityTextField.theme.fontColor = UIColor.darkGray
        cell.mSelectCityTextField.theme.bgColor = UIColor (red: 123.0, green: 200.0, blue: 150.0, alpha: 1.0)
        cell.mSelectCityTextField.theme.borderColor = UIColor (red: 0.9, green: 0.9, blue: 0.9, alpha: 1)
        cell.mSelectCityTextField.theme.separatorColor = UIColor (red: 0.9, green: 0.9, blue: 0.9, alpha: 0.5)
        cell.mSelectCityTextField.theme.cellHeight = 30
        
        cell.mSelectCityTextField.highlightAttributes = [NSBackgroundColorAttributeName: UIColor.lightGray, NSFontAttributeName:UIFont(name: "CourierNewPSMT", size: 20)!] // yellow
        cell.mSelectCityTextField.comparisonOptions = [.caseInsensitive]
        
        //
        cell.mSelectCityTextField.filterStrings(sourceCityArray)
        
        cell.mSelectCityTextField.itemSelectionHandler = { filteredResults, itemPosition in
            let item = filteredResults[itemPosition]
            print("Item at position \(itemPosition): \(item.title)")
            cell.mSelectCityTextField.text = item.title
            self.selectedSourceCityName = cell.mSelectCityTextField.text!
            
            if item.title != ""{
                // list a single key's value
                print("Value for key is = ", self.mutableDictForSourceCity[item.title] ?? "Aurangabad")
                self.selectedSourceCityID = self.mutableDictForSourceCity[item.title] ?? 0
            }
        }
        
        if isFromFinalSearchButton{
            self.selectedSourceCityName =  cell.mSelectCityTextField.text!
            isFromFinalSearchButton = false
            
            if (sourceCityArray.contains(selectedSourceCityName)){
                if mStartDateTextField.text != ""{
                  if mEndDateTextField.text != ""{
                    if mSelectTimeTextField.text != ""{
                        print("selected Source City is valid: \(selectedSourceCityName)")
                        self.callToAPI()
                    }else{
                        AppUtility.sharedInstance.showAlertWithoutIcon(title: "Alert", subTitle: "Please select valid pickup time...")
                    }
                  }else{
                    AppUtility.sharedInstance.showAlertWithoutIcon(title: "Alert", subTitle: "Please select end date...")
                    }
                }else{
                    AppUtility.sharedInstance.showAlertWithoutIcon(title: "Alert", subTitle: "Please select start date...")
                }
            }else{
                AppUtility.sharedInstance.showAlertWithoutIcon(title: "Alert", subTitle: "Please select valid city...")
            }
        }
 */
        //
        return cell
    }
    
    //MARK:- IBAction Methods

    
    
    //MARK:- Instance Methods
    
    @objc func doneButtonAction()  {
        if isFromStartDateButton{
            picker.removeFromSuperview()
            self.isStartDateCalenderOpen = true
            self.doneButton.isHidden = true
            
        }else if isFromEndDateButton{
            picker.removeFromSuperview()
            self.isEndDateCalenderOpen = true
            self.doneButton.isHidden = true
            
        }else{
            timePicker.removeFromSuperview()
            self.isTimePickerOpen = true
            self.doneButton.isHidden = true
        }
    }
    
    func callToAPI()  {
        let calendar = Calendar.current
        let formatter = DateFormatter()
        var newDate = start_date
        daysListArray.removeAll()
        
        while newDate <= end_date {
            formatter.dateFormat = "yyyy-MM-dd"
            daysListArray.append(formatter.string(from: newDate))
            newDate = calendar.date(byAdding: .day, value: 1, to: newDate)!
        }
        
        let dayCount = daysListArray.count
        let travelDate = mStartDateTextField.text!
        let travelEndDate = mEndDateTextField.text!
        let pickupTime = mSelectTimeTextField.text!
        let journeyRoute = "Local -> Fullday -> " + "\(self.selectedSourceCityName) -> " + "\(travelDate) to \(travelEndDate) -> " + "\(dayCount)-days -> " + "\(pickupTime)"
        
        /*
        // move to user to SearchACabViewController
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SearchACabViewController") as! SearchACabViewController
        vc.journey_route = journeyRoute
        vc.comming_vc_name = "fullday"
        vc.mTrip_type_option = "4"
        vc.mTravel_type_option = "2"
        vc.source_city_name = self.selectedSourceCityName
        vc.journey_travel_Date = travelDate
        vc.journey_end_date = travelEndDate
        vc.no_of_days = dayCount
        vc.journey_pickup_time = pickupTime
        self.navigationController?.pushViewController(vc, animated: true)
         */
    }
    
    func addDoneButton()  {
        doneButton = UIButton(frame: CGRect(x: self.view.frame.width - 60, y: (self.view.frame.height/2 + 40), width: 50, height: 21))
//        doneButton.backgroundColor = Colors.buttonThemeColor
        doneButton.setTitle("Done", for: .normal)
        doneButton.addTarget(self, action: #selector(doneButtonAction), for: .touchUpInside)
        self.view.addSubview(doneButton)
    }
    
    func openDatePicker()  {
        view.endEditing(true)
        picker.datePickerMode = UIDatePickerMode.date
        
        picker.frame = CGRect(x: 0.0, y: (self.view.frame.height/2 + 60), width: self.view.frame.width, height: 220.0)
        picker.backgroundColor = UIColor.white
        self.view.addSubview(picker)
        //        self.buttonDone?.isHidden = false
        
        picker.addTarget(self, action: #selector(LocalViewController.dueDateChanged), for: UIControlEvents.valueChanged)
    }
    
    @objc func dueDateChanged(_ sender:UIDatePicker){
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.long
        dateFormatter.timeStyle = DateFormatter.Style.none
        
        let dateObj = sender.date
        
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let new_date = dateFormatter.string(from: dateObj)
        
        let yesterday = Calendar.current.date(byAdding: .day, value: -1, to: Date())
        let formatter3 = DateFormatter()
        formatter3.dateFormat = "yyyy-MM-dd"
        
        if isFromStartDateButton{
            
            start_date = dateObj
            let isGreater = start_date.isGreater1(than: yesterday!)
            
            if isGreater{
                mStartDateTextField.text = new_date
                mSelectTimeTextField.text = ""
                
                if isEndDateSelected{
//                    if let userDefaultEndDate = UserDefaults.standard.value(forKey: Constant.END_DATE) as? Date{
//                        let isUserDefaultEndDateGreater = userDefaultEndDate.isGreater1(than: start_date)
//                        if isUserDefaultEndDateGreater == false{
//                            mEndDateTextField.text = new_date
//                        }
//                    }
                }
            }else{
//                AppUtility.sharedInstance.showAlertWithoutIcon(title: "Alert", subTitle: "Selected date is earlier than current date")
            }
        }
        
        if isFromEndDateButton{
            
            end_date = dateObj
            let isEndDateGreater = end_date.isGreater1(than: start_date)
            
            if isEndDateGreater{
//                UserDefaults.standard.set(end_date, forKey: Constant.END_DATE)
                mEndDateTextField.text = new_date
                
            }else{
//                AppUtility.sharedInstance.showAlertWithoutIcon(title: "Alert", subTitle: "End date is earlier than start date")
            }
        }
    }
    
    func openTimePicker()  {
        view.endEditing(true)
        timePicker.datePickerMode = UIDatePickerMode.time
        timePicker.minuteInterval = 15
        let date = Date()
        let calendar = Calendar.current
        
        let mHour = calendar.component(.hour, from: date)
        let mMinutes = calendar.component(.minute, from: date)
        let mSeconds = calendar.component(.second, from: date)
        print("hours = \(mHour):\(mMinutes):\(mSeconds)")
        
        if Date().compare(start_date) == ComparisonResult.orderedDescending {
            // same day selected  i.e. current date selected
            // set hr and min value more than 4 hr from current hr and min value
            
            let calendar = Calendar.current
            var minDateComponent = calendar.dateComponents([.hour,.minute,.second], from: Date())
            minDateComponent.hour = mHour + 4
            minDateComponent.minute = mMinutes
            minDateComponent.second = mSeconds
            
            let minDate = calendar.date(from: minDateComponent)
            timePicker.minimumDate = minDate! as Date
            timePicker.setDate(minDate!, animated: true)
        }
        else{
            let calendar = Calendar.current
            var minDateComponent = calendar.dateComponents([.hour,.minute,.second], from: Date())
            minDateComponent.hour = 0
            minDateComponent.minute = 0
            minDateComponent.second = 0
            
            let minDate = calendar.date(from: minDateComponent)
            timePicker.minimumDate = minDate! as Date
            timePicker.setDate(minDate!, animated: true)
        }
        
        timePicker.frame = CGRect(x: 0.0, y: (self.view.frame.height/2 + 60), width: self.view.frame.width, height: 220.0)
        timePicker.backgroundColor = UIColor.white
        timePicker.reloadInputViews()
        self.view.addSubview(timePicker)
        timePicker.addTarget(self, action: #selector(LocalViewController.startTimeDiveChanged), for: UIControlEvents.valueChanged)
    }
    
    @objc func startTimeDiveChanged(sender: UIDatePicker) {
        let formatter = DateFormatter()
        formatter.timeStyle = .short
        mSelectTimeTextField.text = formatter.string(from: sender.date)
        //        self.selectedTime = formatter.string(from: sender.date)
    }
}

extension Date {
    func isGreater1(than date: Date) -> Bool {
        return self >= date
    }
    func isSmaller1(than date: Date) -> Bool {
        return self < date
    }
    func isEqual1(to date: Date) -> Bool {
        return self == date
    }
}
