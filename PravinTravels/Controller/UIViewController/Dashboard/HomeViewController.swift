//
//  HomeViewController.swift
//  PravinTravels
//
//  Created by IIPL 5 on 02/01/19.
//  Copyright © 2019 IIPL 5. All rights reserved.
//

import UIKit
import Alamofire

class HomeViewController: UIViewController {

    //MARK:- Variable Declartation
    
    var masterSourceCityArray = [String]()
    var masterMutableDictForSourceCity = [String: Int]()
    
    //MARK:- ViewController LifeCycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Book a Cab"
        
//        if AppUtility.sharedInstance.mIsCityListActive{
//            self.getAllCityAPICall()
//        }
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CityListViewController") as! CityListViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        
        let menuButton = self.navigationItem.leftBarButtonItem
        
        if self.revealViewController() != nil {
            menuButton?.target = self.revealViewController()
            menuButton?.action = #selector(SWRevealViewController.revealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        }
    }
    
    //MARK:- Instance Methods
    
    func makeAPhoneCall()  {
        let url: NSURL = URL(string: "TEL://7620368331")! as NSURL//8888855220
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
        } else {
            // Fallback on earlier versions
        }
    }
    
    // MARK:- API CALL Methods
    
    func getAllCityAPICall(){
        
        if AppUtility.sharedInstance.mIsNetworkAvailable{
            
            let todosEndpoint: String = Constant.BASE_URL + Constant.GET_CITY_LIST
            
            let escapedString = todosEndpoint.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)
            
            AppUtility.sharedInstance.showProgressIndicatorWith(message: "Processing...")
            
            Alamofire.request(escapedString!, method: .get, encoding: JSONEncoding.default)
                .responseJSON { response in
                    debugPrint(response)
                    
                    AppUtility.sharedInstance.hideProgressIndicatorWith()
                    
                    if let data = response.result.value{
                        
                        if  let result = (data as? [String : AnyObject])?["result"]{
                            if let dictionaryArray = result as? Array<Dictionary<String, AnyObject?>> {
                                if dictionaryArray.count > 0 {
                                    
                                    var city_Id = Int()
                                    var city_name = ""
                                    
                                    for i in 0..<dictionaryArray.count{
                                        
                                        let Object = dictionaryArray[i]
                                        if let cityId = Object["cityId"] as? Int{
                                            city_Id = cityId
                                        }
                                        if let ctname = Object["ctname"] as? String{
                                            city_name = ctname
                                            self.masterSourceCityArray.append(city_name)
                                        }
                                        self.masterMutableDictForSourceCity[city_name] = city_Id
                                    }
                                    
                                    AppUtility.sharedInstance.mIsCityListActive = false
                                    // store values in CCRUserDAO Class
                                    PUserDAO.sharedInstance.saveMasterSourceCityArray(self.masterSourceCityArray)
                                    PUserDAO.sharedInstance.saveMasterSourceCityMutableDictionary(self.masterMutableDictForSourceCity)
                                }
                            }
                        }
                    }
                    else {
                        let error = (response.result.value  as? [[String : AnyObject]])
                        print(error as Any)
                    }
            }
        }else{
            AppUtility.sharedInstance.showAlertWithoutIcon(title: "Internet not available", subTitle: "Please try after sometime...")
        }
    }

}
